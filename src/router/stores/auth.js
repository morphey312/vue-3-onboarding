import {defineStore} from "pinia";
import axiosInstance from "../../../services/axios.js"

export const useAuthStore = defineStore('auth', {
    state: () => ({
        usersState: {},
        user: {},
        token: null,
    }),
    getters: {
        users: (state) => state.usersState,
    },
    actions: {
        getUsers() {
            axiosInstance.get('auth/user', {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => {
                    if (response.status !== 200) {
                        localStorage.setItem('token', null);
                    } else {
                        this.user = response.data.data;
                    }
                })
        },
        loginUser(data) {
            axiosInstance.post('/auth/login', {
                ...data
            }).then((response) => {
                this.token = response.data.token;
                this.user= response.data.user;
                localStorage.setItem('token', this.token);
            })

            this.user = this.usersState.find(user => user.id === 5);
        },
        logout() {
            axiosInstance.post('/auth/logout')
                .then((response) => {
                    this.user = {};
                    this.token = null;
                    localStorage.setItem('token', null);
                })
        }
    }
})