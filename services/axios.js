import axios from "axios";

const instance = axios.create({
    baseURL: 'https://127.0.0.1:444/api',
    headers: {'Content-Type': 'application/json'}
});

export default instance;