import Pusher from 'pusher-js';

const appKey = '82b27b4481391efb60a9';
const cluster = 'eu';
// const authStore = useAuthStore();
const pusher = new Pusher(appKey, {
    cluster: cluster,
});

// Enable pusher logging - don't include this in production
pusher.logToConsole = true;

// SUBSCRIBE TO TERMINAL EVENTS
// const channel = pusher.subscribe('private-pos.terminal.{{ $id }}');

// channel.bind('payment.succeed', function(data) {
//  ** do something with data**;
// });
// channel.bind('payment.declined', function(data) {
//  ** do something with data**;
// });

export { pusher};